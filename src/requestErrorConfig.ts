﻿import { getToken } from '@/utils/auth-utils';
import { errorTip, isSuccess } from '@/utils/common-utils';
import type { RequestOptions } from '@@/plugin-request/request';
import type { RequestConfig } from '@umijs/max';

// 与后端约定的响应数据格式
interface ResponseStructure {
  code?: number;
  msg?: string;
  data: any;
}

/**
 * pro 自带的错误处理， 可以在这里做自己的改动
 * @doc https://umijs.org/docs/max/request#配置
 */
export const errorConfig: RequestConfig = {
  // 错误处理： umi@3 的错误处理方案。
  errorConfig: {
    // 错误抛出
    errorThrower: (res) => {
      const { data, code, msg } = res as unknown as ResponseStructure;
      if (!isSuccess(res)) {
        const error: any = new Error(msg);
        error.name = 'BizError';
        error.info = { code, msg, data };
        throw error; // 抛出自制的错误
      }
    },
    // 错误接收及处理
    errorHandler: (error: any, opts: any) => {
      if (opts?.skipErrorHandler) throw error;
      // 我们的 errorThrower 抛出的错误。
      if (error.name === 'BizError') {
        console.info(error);
      } else if (error.response) {
        // Axios 的错误
        // 请求成功发出且服务器也响应了状态码，但状态代码超出了 2xx 的范围
        errorTip(`Response status:${error.response.status}`);
      } else if (error.request) {
        // 请求已经成功发起，但没有收到响应
        // \`error.request\` 在浏览器中是 XMLHttpRequest 的实例，
        // 而在node.js中是 http.ClientRequest 的实例
        errorTip('None response! Please retry.');
      } else {
        // 发送请求时出了点问题
        errorTip('Request error, please retry.');
      }
    },
  },

  // 请求拦截器
  requestInterceptors: [
    (config: RequestOptions) => {
      // 拦截请求配置，进行个性化处理。
      config.headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + getToken(),
      };
      config.timeout = 5000;
      return { ...config };
    },
  ],

  // 响应拦截器
  responseInterceptors: [
    (response: any) => {
      if (response.data.code !== 200) {
        /*if (response.data.code === 401) {
          removeToken();
          errorTip('登录超时,请重新登录');
          history.replace({
            pathname: '/user/login',
          });
        } else {
          errorTip(response.data.msg);
        }*/
      }
      return response;
    },
  ],
};

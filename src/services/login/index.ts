// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

export async function loginReq(body: API.LoginParams, options?: { [key: string]: any }) {
  return request<any>('/api/login', {
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}

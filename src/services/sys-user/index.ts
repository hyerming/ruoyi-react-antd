// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

export async function getUserInfoReq() {
  return request<any>('/api/getInfo', {
    method: 'GET',
    params: {},
  });
}

export async function getUserListReq(data: any) {
  return request<any>('/api/system/user/list', {
    method: 'GET',
    params: data,
  });
}

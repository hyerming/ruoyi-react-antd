// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

export async function getDeptTreeReq() {
  return request<any>('/api/system/user/deptTree', {
    method: 'GET',
    params: {},
  });
}

import { message } from 'antd';

export function isSuccess(res: any) {
  return res && res.code === 200;
}

export function successTip(msg: string) {
  message.success(msg);
}

export function errorTip(msg: string) {
  message.error(msg);
}

export function warningTip(msg: string) {
  message.warning(msg);
}

export function createUUID(length: number = 32) {
  const temp_url = URL.createObjectURL(new Blob());
  const uuid = temp_url.toString();
  URL.revokeObjectURL(temp_url);
  const full = uuid.substring(uuid.lastIndexOf('/') + 1);
  return full.substring(0, length);
}

import { useRouteData } from 'umi';

export function getCurrentRouterName() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const data = useRouteData();
  // @ts-ignore
  return data?.route?.name;
}

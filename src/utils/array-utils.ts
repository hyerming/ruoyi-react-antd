/**
 * 校验是否为空数组
 * @param array
 * @returns {boolean}
 */
export function arrayIsNotEmpty(array: any) {
  return array && array.length > 0;
}

/**
 * 数组里根据某个字段排序
 * @param field
 * @returns {function(*, *)}
 */
export function sortBy(field: any) {
  return (x: { [x: string]: number }, y: { [x: string]: number }) => {
    return x[field] - y[field];
  };
}

import React, { useEffect } from 'react';
// @ts-ignore
import * as THREE from 'three';
// @ts-ignore
import { Line2 } from 'three/examples/jsm/lines/Line2';
// @ts-ignore
import { LineGeometry } from 'three/examples/jsm/lines/LineGeometry';
// @ts-ignore
import { arrayIsNotEmpty } from '@/utils/array-utils';
// @ts-ignore
import { STATION_POINT_TYPE, STRAIGHT_LINE_TYPE, THREE_CONFIG } from '@/config/three-config';
// @ts-ignore
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';
// @ts-ignore
import { createUUID } from '@/utils/common-utils';
// @ts-ignore
import { LINE_LIST, POINT_LIST } from '@/pages/Threejs/demo3/data';
// @ts-ignore
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial';
import html2canvas from 'html2canvas';




const WIDTH = window.innerWidth - 280;
const HEIGHT = window.innerHeight - 120;

const Demo3: React.FC = () => {
  let raycaster: any;
  let scene: any;
  let camera: any;
  let renderer: any;
  let container: any;
  const pointer = new THREE.Vector2();

  const onWindowResizeHandle = () => {
    //renderer.setSize(window.innerWidth - 280, window.innerHeight - 120);
  };

  const cancalClickedHandle = () => {
    if (arrayIsNotEmpty(scene.children)) {
      for (let child of scene.children) {
        child?.material?.color?.set(THREE_CONFIG.DEFAULT_COLOR);
      }
      renderer.render(scene, camera);
    }
  };

  const updatePointerHandle = (event: any) => {
    let getBoundingClientRect = container.getBoundingClientRect();
    let px = getBoundingClientRect.left;
    let py = getBoundingClientRect.top;
    pointer.x = ((event.clientX - px) / renderer.domElement.clientWidth) * 2 - 1;
    pointer.y = -((event.clientY - py) / renderer.domElement.clientHeight) * 2 + 1;
  };

  const onMouseClickHandle = (event: any) => {
    updatePointerHandle(event);
    raycaster.setFromCamera(pointer, camera);
    const intersects = raycaster.intersectObjects(scene.children, true);
    cancalClickedHandle();
    if (arrayIsNotEmpty(intersects)) {
      console.info('click', intersects);
      intersects[0]?.object?.material?.color?.set(THREE_CONFIG.CLICKED_COLOR);
      renderer.render(scene, camera);
    } else {
      cancalClickedHandle();
    }
  };

  const onMouseMoveHandle = (event: any) => {
    updatePointerHandle(event);
    raycaster.setFromCamera(pointer, camera);
    const intersects = raycaster.intersectObjects(scene.children, true);
    if (arrayIsNotEmpty(intersects)) {
      document.body.style.cursor = 'pointer';
    } else {
      document.body.style.cursor = 'default';
    }
  };

  const render = () => {
    camera.position.set(0, 40, 100);
    camera.lookAt(scene.position);
    camera.updateMatrixWorld();
    renderer.render(scene, camera);
  };

  const addPoint = (pointList: Array<any>) => {
    if (arrayIsNotEmpty(pointList)) {
      for (let item of pointList) {
        const geometry = new THREE.SphereGeometry(2.5);
        const material = new THREE.MeshBasicMaterial({ color: THREE_CONFIG.DEFAULT_COLOR });
        const point = new THREE.Mesh(geometry, material);
        point.position.set(item.x, item.y, item.z);
        point.userData = {
          id: createUUID(),
          type: STATION_POINT_TYPE,
          x: item.x,
          y: item.y,
          z: item.z,
        };
        scene.add(point);
      }
    }
  };

  const addStraightLineHandle = (startPoint: any, endPoint: any) => {
    let geometry = new LineGeometry();
    geometry.setPositions(
      [startPoint.x, startPoint.y, startPoint.z].concat(endPoint.x, endPoint.y, endPoint.z),
    );
    let material = new LineMaterial({
      color: THREE_CONFIG.DEFAULT_COLOR,
      linewidth: 10,
    });
    material.resolution.set(WIDTH, HEIGHT);
    let line = new Line2(geometry, material);
    line.userData = {
      id: createUUID(),
      type: STRAIGHT_LINE_TYPE,
      startPoint,
      endPoint,
    };
    line.computeLineDistances();
    scene.add(line);
  };

  const init = () => {
    container = document.getElementById('webgl');
    const tagElement = document.getElementById('tag')
    console.info('tagElement',tagElement)


    if (container) {
      container.innerHTML = '';
    }
    camera = new THREE.PerspectiveCamera(70, WIDTH / HEIGHT, 1, 10000);

    // 创建3D场景对象Scene
    scene = new THREE.Scene();

    // 添加直线
    addStraightLineHandle(LINE_LIST.startPoint, LINE_LIST.endPoint);

    // 添加点
    addPoint(POINT_LIST);

    raycaster = new THREE.Raycaster();
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(WIDTH, HEIGHT);
    renderer.setClearColor('#ffffff', 1);
    container.appendChild(renderer.domElement);

    document.addEventListener('click', onMouseClickHandle);
    document.addEventListener('mousemove', onMouseMoveHandle);
    window.addEventListener('resize', onWindowResizeHandle);

    const controls = new OrbitControls(camera, renderer.domElement);
    controls.addEventListener('change', function () {
      renderer.render(scene, camera);
    });


    if (tagElement) {
      tagElement.innerHTML = '站点1'
      html2canvas(tagElement).then(canvas => {
        let texture = new THREE.CanvasTexture(canvas);
        let material = new THREE.SpriteMaterial({
          map: texture,
          color:'#FFFFFF'
        });
        let sprite = new THREE.Sprite(material);
        sprite.scale.set(1, 1, 1);
        sprite.position.set(10, 0, 0);
        scene.add(sprite)
        renderer.render(scene, camera);
      });
    }
    render();
  };

  useEffect(() => {
    init();
  }, []);
  return (
      <div id="webgl">
      </div>
  );
};
export default Demo3;

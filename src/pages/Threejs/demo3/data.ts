export const POINT_LIST = [
  {
    x: 0.1,
    y: 0.1,
    z: 0.1,
  },
  {
    x: 20,
    y: 20,
    z: 20,
  },
];

export const LINE_LIST = {
  startPoint: {
    x: 0.1,
    y: 0.1,
    z: 0.1,
  },
  endPoint: {
    x: 20,
    y: 20,
    z: 20,
  },
};

import CustomIcon from '@/components/CustomIcon';
import { theme } from 'antd';
import React from 'react';
const { useToken } = theme;

const Welcome: React.FC = () => {
  const { token } = useToken();
  return (
    <div>
      111
      <CustomIcon color="red" type="home"></CustomIcon>
    </div>
  );
};

export default Welcome;

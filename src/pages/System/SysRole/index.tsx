import { getCurrentRouterName } from '@/utils/router-utils';
import { Card } from 'antd';
import React from 'react';

const SysRole: React.FC = () => {
  return (
    <Card title={getCurrentRouterName()}>
      <div></div>
      <p>Card content</p>
      <p>Card content</p>
      <p>Card content</p>
    </Card>
  );
};

export default SysRole;

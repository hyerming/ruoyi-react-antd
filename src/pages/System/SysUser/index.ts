export interface SysUserQueryType {
  nickName: string;
  userName: string;
  remark: string;
}

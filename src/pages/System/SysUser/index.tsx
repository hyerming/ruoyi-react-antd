import CustomPagination from '@/components/CustomPagination';
import DeptTree from '@/components/DeptTree';
import { DEFAULT_PAGE } from '@/config/page-config';
import { QUERY_FORM_STYLE } from '@/config/query-form-config';
import { SysUserQueryType } from '@/pages/System/SysUser/index';
import { getUserListReq } from '@/services/sys-user';
import type { PageType } from '@/types/global';
import { isSuccess } from '@/utils/common-utils';
import { getCurrentRouterName } from '@/utils/router-utils';
import { useRequest } from 'ahooks';
import { Button, Card, Col, Form, Input, Row, Select, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import type { DataNode } from 'antd/es/tree';
import React, { useState } from 'react';

const columns: ColumnsType<SysUserQueryType> = [
  {
    title: '姓名',
    dataIndex: 'nickName',
  },
  {
    title: '登录名',
    dataIndex: 'userName',
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
];

const treeData: DataNode[] = [
  {
    title: 'parent 1',
    key: '0-0',
    children: [
      {
        title: 'parent 1-0',
        key: '0-0-0',
        disabled: true,
        children: [
          {
            title: 'leaf',
            key: '0-0-0-0',
            disableCheckbox: true,
          },
          {
            title: 'leaf',
            key: '0-0-0-1',
          },
        ],
      },
      {
        title: 'parent 1-1',
        key: '0-0-1',
        children: [{ title: <span style={{ color: '#1890ff' }}>sss</span>, key: '0-0-1-0' }],
      },
    ],
  },
];

const SysUser: React.FC = () => {
  const [form] = Form.useForm();
  const [formLayout] = useState<any>('inline');
  const [tableList, setTableList] = useState([]);
  const [total, setTotal] = useState(0);
  const [page, setPage] = useState<PageType>(Object.assign({}, DEFAULT_PAGE));

  const { run, loading } = useRequest(
    (pageParam, formParam) => getUserListReq({ ...pageParam, ...formParam }),
    {
      defaultParams: [{ ...DEFAULT_PAGE }, {}],
      onSuccess: (result, params) => {
        if (isSuccess(result)) {
          setTableList(result.rows);
          // @ts-ignore
          setPage(params[0]);
          setTotal(result.total);
        }
      },
    },
  );

  const startQuery = (pageParam: PageType) => {
    const formParam = form.getFieldsValue(true);
    run(pageParam, formParam);
  };

  const handlePageChange = (pageNum: number, pageSize: number) => {
    startQuery({ pageNum, pageSize });
  };

  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: any[]) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
  };

  const onSelect = (selectedKeys: any, info: any) => {
    console.log('selected', selectedKeys, info);
  };

  return (
    <Row gutter={16}>
      <Col span={4}>
        <DeptTree treeData={treeData} onSelect={onSelect}></DeptTree>
      </Col>
      <Col span={20}>
        <Card title={getCurrentRouterName()}>
          <div className="custom-table-query">
            <Form layout={formLayout} form={form}>
              <Form.Item label="登录名" name="userName">
                <Input allowClear style={QUERY_FORM_STYLE} placeholder="请输入登录名" />
              </Form.Item>
              <Form.Item label="手机号" name="phonenumber">
                <Input allowClear style={QUERY_FORM_STYLE} placeholder="请输入手机号" />
              </Form.Item>
              <Form.Item label="状态" name="status">
                <Select
                  allowClear
                  style={QUERY_FORM_STYLE}
                  options={[
                    { value: '0', label: '正常' },
                    { value: '1', label: '停用' },
                  ]}
                />
              </Form.Item>
              <Form.Item>
                <Button type="primary" onClick={() => startQuery({ ...page, pageNum: 1 })}>
                  查询
                </Button>
                <Button className="reset-button" onClick={() => form.resetFields()}>
                  重置
                </Button>
              </Form.Item>
            </Form>
          </div>
          <div className="custom-table-result">
            <Table
              size={'small'}
              rowKey="userId"
              loading={loading}
              rowSelection={rowSelection}
              columns={columns}
              dataSource={tableList}
              pagination={false}
            />
            <CustomPagination
              current={page.pageNum}
              pageSize={page.pageSize}
              total={total}
              onChange={handlePageChange}
            />
          </div>
        </Card>
      </Col>
    </Row>
  );
};

export default SysUser;

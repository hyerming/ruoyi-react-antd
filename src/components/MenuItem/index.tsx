import { MenuDataItem } from '@ant-design/pro-layout';
import { theme } from 'antd';
import React from 'react';
import { Link } from 'umi';
const { useToken } = theme;

const MenuItem: React.FC<MenuDataItem> = (menuItemProps) => {
  const { token } = useToken();
  const { isUrl: isLink, path, icon, name } = menuItemProps;
  const itemContent = (
    <span className="ant-pro-menu-item">
      <span className="ant-pro-menu-item-title" style={{ color: token.colorTextBase }}>
        {name}
      </span>
    </span>
  );
  return isLink || !path || location.pathname === path ? (
    itemContent
  ) : (
    <Link to={path}>{itemContent}</Link>
  );
};

export default MenuItem;

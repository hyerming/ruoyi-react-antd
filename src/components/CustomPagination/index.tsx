import { Pagination } from 'antd';
import React from 'react';

interface CustomPaginationProps {
  current: number;
  pageSize: number;
  total: number;
  onChange?: (pageNum: number, pageSize: number) => void;
}

const CustomPagination: React.FC<CustomPaginationProps> = (props) => {
  const { current, pageSize, total, onChange } = props;
  return (
    <div className="custom-page">
      <Pagination
        current={current}
        defaultPageSize={pageSize}
        total={total}
        showSizeChanger
        showQuickJumper
        showTotal={(value) => `共 ${value} 条`}
        onChange={onChange}
      />
    </div>
  );
};

export default CustomPagination;

import { Tree } from 'antd';
import React from 'react';

interface DeptTreeProps {
  treeData: any;
  onSelect?: (selectedKeys: any, info: any) => void;
}

const DeptTree: React.FC<DeptTreeProps> = (props) => {
  const { treeData, onSelect } = props;
  return (
    <Tree
      defaultExpandedKeys={['0-0-0', '0-0-1']}
      defaultSelectedKeys={['0-0-0', '0-0-1']}
      defaultCheckedKeys={['0-0-0', '0-0-1']}
      onSelect={onSelect}
      treeData={treeData}
    />
  );
};

export default DeptTree;

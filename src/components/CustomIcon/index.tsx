import { createFromIconfontCN } from '@ant-design/icons';
import React from 'react';
import defaultSettings from '../../../config/defaultSettings';

interface CustomPaginationProps {
  type: string;
  color?: string;
  fontSize?: number;
  height?: number;
  style?: React.CSSProperties;
}

const CustomIcon: React.FC<CustomPaginationProps> = (props) => {
  const { type, style, fontSize, color } = props;

  const MyIcon = createFromIconfontCN({
    scriptUrl: defaultSettings.iconfontUrl,
    extraCommonProps: {
      style: { ...style, fontSize, color },
    },
  });
  return <MyIcon type={`icon-${type}`}></MyIcon>;
};

CustomIcon.defaultProps = {
  fontSize: 14,
};

export default CustomIcon;

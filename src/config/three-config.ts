// Three基本配置
export const THREE_CONFIG = {
  POINT_COLOR: '#67a6ff',
  LINE_COLOR: '#67a6ff',
  DEFAULT_COLOR: '#67a6ff',
  CLICKED_COLOR: '#1677ff',
};
// 直线
export const STRAIGHT_LINE_TYPE = 'straight-line';
// 曲线
export const CURVE_LINE_TYPE = 'curve-line';
// 站点
export const STATION_POINT_TYPE = 'station-point';

export default [
  {
    authority: '',
    hideChildrenInMenu: false,
    hideInMenu: false,
    icon: 'icon-home',
    name: '欢迎页',
    path: '/welcome',
  },
  {
    authority: '',
    icon: 'icon-border-outer',
    name: 'Threejs',
    path: '/threejs',
    children: [
      {
        authority: '',
        hideChildrenInMenu: false,
        hideInMenu: false,
        name: 'Demo1',
        path: '/threejs/demo1',
      },
      {
        authority: '',
        hideChildrenInMenu: false,
        hideInMenu: false,
        name: 'Demo2',
        path: '/threejs/demo2',
      },
      {
        authority: '',
        hideChildrenInMenu: false,
        hideInMenu: false,
        name: 'Demo3',
        path: '/threejs/demo3',
      },
    ],
  },
  {
    authority: '',
    icon: 'icon-setting',
    name: '系统管理',
    path: '/system',
    children: [
      {
        authority: '',
        hideChildrenInMenu: false,
        hideInMenu: false,
        name: '用户列表',
        path: '/system/sys-user',
      },
      {
        authority: '',
        hideChildrenInMenu: false,
        hideInMenu: false,
        name: '角色列表',
        path: '/system/sys-role',
      },
    ],
  },
];
